# PyMacroRecorder
![Yo Dawg](yodawg.png)

This macro recorder records a macro as a new python script called macro.py. The new script can then be shared with anyone and used for UI testing, assigned to a hotkey or any other purpose that a macro would typically be used for. As of right now even though the new script is python it isn't easily readable or modifiable. The end game is to eventually make the output macro script easily readable and modifiable by anyone that uses it even if they are not familiar with python or even programming in general.

NOTE: Mouse wheel stuff is broken. That is because of the keyboard library not me! I issued a pull request so hopefully that gets resolved soon.

## Instructions
1. Install python if you don't have it installed already. There are too many options to list here for doing this. Do some research on your own to find the option that is most suitable for you based on your individual needs and OS requirements.

2. Install the keyboard python module if you don't have it installed already
```
pip install keyboard
```
3. Then run the pymacrorec.py script. Use the "esc" key to stop recording.
```
python pymacrorec.py
```
4. Once you have finished recording by pressing the "esc" key then you can run the new macro.py to replay your macro.
```
python macro.py
```

## To Do
  * Add more code comments.
  * Better code organization to make things more readable and modifiable.
  * Combine mouse move events and timer to eliminate unnecessary movements in the macro
    * or better yet make that a configurable option.
  * Test on OSes other than Windows and make sure this works on each of them.
  * Include options for configurability within the macro.
    * Macro repeat
    * Speed factor
  * If macro.py already exists name the output file macro#.py where # is the next number that doesn't exist yet.
  * The scriptify function can definitely be broken up into more and simpler functions.
