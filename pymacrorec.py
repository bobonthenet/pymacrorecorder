import keyboard, time, platform
from keyboard import mouse

###################CUSTOMIZE THE RECORDER BY CHANGING THESE VARIABLES#######
STOP_RECORDING_KEY = 'esc'
KEYPRESS_COMBINE = True
MOUSE_MOVEMENT_COMBINE = False #Doesn't work. I'm just putting this here to help with planning future enhancements.
############################################################################

def main():
    print('press {0} key to end recording'.format(STOP_RECORDING_KEY))

    recorded = []
    keyboard.hook(recorded.append)
    mouse.hook(recorded.append)

    keyboard.wait(combination = STOP_RECORDING_KEY)

    keyboard.unhook(recorded.append)
    mouse.unhook(recorded.append)

    script_init()
    scriptify(recorded)

def write_shift(key):
    shift_dict = {"1":"!",
                  "2":"@",
                  "3":"#",
                  "4":"$",
                  "5":"%",
                  "6":"^",
                  "7":"&",
                  "8":"*",
                  "9":"(",
                  "0":")",
                  "-":"_",
                  "=":"+",
                  "`":"~",
                  "[":"{",
                  "]":"}",
                  "\\":"|",
                  ";":":",
                  "'":"\"",
                  ",":"<",
                  ".":">",
                  "/":"?"}

    if key.isalpha():
        return key.upper()
    else:
        return shift_dict[key]

def script_init():
    macroFile.write('import keyboard, time, platform\n')
    macroFile.write('from keyboard import mouse\n')
    #TODO:Write out some comments here explaining how this file can be modified.
    macroFile.write('print(\'press enter key to play macro\')\n')
    macroFile.write('keyboard.wait(\'enter\')\n')
    macroFile.write('speed_factor=1.0\n')
    macroFile.write('keyState = keyboard.stash_state()\n\n')

def scriptify(recording):
    last_time = None
    #TODO: Break up special_keys list into more meaningful lists.
    special_keys = ['right shift', 'left shift', 'esc', 'enter', 'left ctrl', 'right ctrl', 'delete', 'backspace']
    write = ''
    shift_down = False
    ctrl_down = False
    wait_total = 0
    sleepTime = .01

    for event in recording:
        if last_time is not None:
            sleepTime = (event.time - last_time)
        last_time = event.time
        if isinstance(event, keyboard.KeyboardEvent):
            key = event.name or event.scan_code
            if KEYPRESS_COMBINE:
                #TODO: Try and reduce the number of nested if statements.
                if event.event_type == keyboard.KEY_DOWN:
                    if key == STOP_RECORDING_KEY:
                        wait_total += sleepTime
                        macroFile.write('keyboard.write(\'{0}\', delay={1}/speed_factor, restore_state_after=False)\n'.format(write, wait_total / len(write)))
                    elif key in special_keys:
                        if key in ['right shift', 'left shift']:
                            shift_down = True
                        elif key in ['left ctrl', 'right ctrl']:
                            if len(write) > 0:
                                wait_total += sleepTime
                                macroFile.write('keyboard.write(\'{0}\', delay={1}/speed_factor, restore_state_after=False)\n'.format(write, wait_total / len(write)))
                                wait_total = 0
                                write = ''
                            macroFile.write('keyboard.press(\'{0}\')\n'.format(key))
                            ctrl_down = True
                        elif key in ['esc', 'enter']:
                            if len(write) > 0:
                                wait_total += sleepTime
                                macroFile.write('keyboard.write(\'{0}\', delay={1}/speed_factor, restore_state_after=False)\n'.format(write, wait_total / len(write)))
                                wait_total = 0
                                write = ''
                            macroFile.write('keyboard.press(\'{0}\')\n'.format(key))
                        else:
                            macroFile.write('time.sleep({0})\n'.format(sleepTime))
                            macroFile.write('keyboard.press(\'{0}\')\n'.format(key))
                    else:
                        if ctrl_down:
                            macroFile.write('keyboard.press(\'{0}\')\n'.format(key))
                        elif shift_down:
                            if key == 'space':
                                write += ' '
                            else:
                                write += write_shift(key)
                        else:
                            if key == 'space':
                                write += ' '
                            elif key == '\'':
                                write += '\\\''
                            else:
                                write += key
                elif event.event_type == keyboard.KEY_UP:
                    if ctrl_down:
                        if key in ['right ctrl', 'left ctrl']:
                            ctrl_down = False
                        macroFile.write('keyboard.release(\'{0}\')\n'.format(key))
                    elif key in ['right shift', 'left shift']:
                        shift_down = False
                    elif key in special_keys:
                        macroFile.write('keyboard.release(\'{0}\')\n'.format(key))
                        if len(write) > 0:
                            wait_total += sleepTime
                            macroFile.write('keyboard.write(\'{0}\', delay={1}/speed_factor, restore_state_after=False)\n'.format(write, wait_total / len(write)))
                            wait_total = 0
                            write = ''
            else: #KEYPRESS_COMBINE == False
                if event.event_type == keyboard.KEY_DOWN:
                    macroFile.write('time.sleep({0}/speed_factor)\n'.format(sleepTime))
                    macroFile.write('keyboard.press(\'{0}\')\n'.format(key))
                elif event.event_type == keyboard.KEY_UP:
                    macroFile.write('time.sleep({0}/speed_factor)\n'.format(sleepTime))
                    macroFile.write('keyboard.release(\'{0}\')\n'.format(key))
        elif isinstance(event, mouse.ButtonEvent):
            if event.event_type == mouse.UP:
                macroFile.write('mouse.release(\'{0}\')\n'.format(event.button))
            else:
                macroFile.write('mouse.press(\'{0}\')\n'.format(event.button))
        elif isinstance(event, mouse.MoveEvent):
            #TODO:If MOUSE_MOVEMENT_COMBINE == true only write the last movement before another event. Add up the time to use for delay.
            if len(write) > 0:
                wait_total += sleepTime
                macroFile.write('keyboard.write(\'{0}\', delay={1}/speed_factor, restore_state_after=False)\n'.format(write, wait_total / len(write)))
                wait_total = 0
                write = ''
            macroFile.write('time.sleep({0}/speed_factor)\n'.format(sleepTime))
            macroFile.write('mouse.move({0}, {1})\n'.format(event.x, event.y))
        elif isinstance(event, mouse.WheelEvent):
            if len(write) > 0:
                wait_total += sleepTime
                macroFile.write('keyboard.write(\'{0}\', delay={1}/speed_factor, restore_state_after=False)\n'.format(write, wait_total / len(write)))
                wait_total = 0
                write = ''
            macroFile.write('time.sleep({0}/speed_factor)\n'.format(sleepTime))
            macroFile.write('mouse.wheel({0})\n'.format(event.delta))

    macroFile.write('keyboard.restore_state(keyState)\n')
    macroFile.close()

if __name__ == "__main__":
    macroFile = open('macro.py', 'w')
    main()
